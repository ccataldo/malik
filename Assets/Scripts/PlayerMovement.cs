﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour {

	public Animator animFeet;
	public float speed = 5;

	public Transform limitLeft;
	public Transform limitRight;
	public Transform limitTop;
	public Transform limitBottom;

	private float horizontalMove = 0f;
	private float verticalMove = 0f;
	private bool facingRight = true;

	public void Start(){

		Physics.autoSimulation = false;
	}

	public void HandleMovement(float axisRawX, float axisRawY){

		if (axisRawX == 0 && axisRawY == 0) {
			StopPlayer ();
			return;
		}

		speed = 5;

		horizontalMove = axisRawX * speed;
		verticalMove = axisRawY * (speed / 2);

		float xPos = Mathf.Clamp(transform.position.x + (Time.deltaTime * horizontalMove), limitLeft.position.x, limitRight.position.x);
		float yPos = Mathf.Clamp(transform.position.y + (Time.deltaTime * verticalMove), limitBottom.position.y, limitTop.position.y);

		Vector3 posClamped = new Vector3 (xPos, yPos, transform.position.z);
		transform.position = posClamped;

		animFeet.SetBool ("isWalking", true);

		Flip (axisRawX);
	}

    public void MoveLeft(){
		animFeet.SetBool ("isWalking", true);
		Flip (-1);
		speed = -5;
    }

    public void MoveRight()
    {
		Flip (1);
		animFeet.SetBool ("isWalking", true);
		speed = 5;
    }

	public void StopPlayer(){

		speed = 0;
		animFeet.SetBool ("isWalking", false);
	}

	public void Flip(float axis){

		if (axis > 0 && !facingRight || axis < 0 && facingRight) {

			facingRight = !facingRight;

			Vector3 newScale = transform.localScale;

			newScale.x *= -1;

			transform.localScale = newScale;
		}
	}
}
