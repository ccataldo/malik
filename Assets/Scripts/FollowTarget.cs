﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowTarget : MonoBehaviour {

	public GameObject target;
	public float speed = .1f;

	public float limitLeft;
	public float limitRight;

	void Update () {

		float xPos = Mathf.Clamp(target.transform.position.x, limitLeft, limitRight);

		Vector3 posClamped = new Vector3 (xPos, transform.position.y, transform.position.z);

		transform.position = Vector3.Lerp (transform.position, posClamped, speed);
	}
}
