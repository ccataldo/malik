﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerInput : MonoBehaviour {

	private PlayerMovement c_movement;
	private PlayerShoot c_shoot;
	// Use this for initialization
	public void Start () {
		
		c_movement = GetComponent<PlayerMovement> ();
		c_shoot = GetComponent<PlayerShoot>();
	}
	
	// Update is called once per frame
	public void Update () {

		c_movement.HandleMovement (Input.GetAxisRaw ("Horizontal"), Input.GetAxisRaw("Vertical"));

		/*if (Input.touchCount > 0)
        {
            Touch touch = Input.GetTouch(0);

			if (touch.phase == TouchPhase.Began)
			{
				if (touch.position.x < Screen.width / 2)
					c_movement.MoveLeft();
				else
					c_movement.MoveRight();
			}

		   
            if(touch.phase == TouchPhase.Ended){
                c_movement.StopPlayer();
            }
        }*/

		if (Input.GetButton("Fire1")) {
			c_shoot.OnShoot ();
		}

		if (Input.GetButtonUp ("Fire1")) {
			c_shoot.StopShooting ();
		}
	}
}
