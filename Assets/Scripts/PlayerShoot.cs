﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerShoot : MonoBehaviour {

	public Transform weaponTip;

	public float fireRate = 5f;
	public float damage = 25f;
	public float range = 100f;
	public LayerMask whatToHit;

	public float timeFire = 0;

	private Animator animFire;
	private Animator animBody;

	public void Start(){

		animBody = GetComponent<Animator> ();
		animFire = weaponTip.transform.GetChild (0).gameObject.GetComponent<Animator> ();
	}

	public void OnShoot(){

		if (fireRate == 0) {
			
			Shoot ();
		} else {
			
			if (Time.time > timeFire) {
				
				timeFire = Time.time + 1 / fireRate;
				Shoot ();
			}
		}
	}

	public void Shoot(){

		Vector2 firePos = new Vector2 (weaponTip.position.x, weaponTip.position.y);

		Vector2 dir = Vector2.right;

		if (!animBody.GetBool ("isShooting"))
			animBody.SetBool ("isShooting", true);
		if(!animFire.GetBool("fire"))
			animFire.SetBool ("fire", true);
	}

	public void StopShooting(){

		animFire.SetBool ("fire", false);
		animBody.SetBool ("isShooting", false);
	}
}
